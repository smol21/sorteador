import { defineStore } from "pinia";
import { API_COBRANZA, API_GENERAL } from "../api/urlBackend.js";
import { useLoading } from "vue-loading-overlay";

export const useGeneralStore = defineStore("general", {
  state: () => ({
    slistProduct: [],
  }),
  actions: {
    async listProduct() {
      const $loading = useLoading();
      const loader = $loading.show({
        loader: "dots",
        color: "#0C7F08",
        width: 100,
        height: 100,
      });
      fetch(`${API_GENERAL}ListarProducto/MDM=/MDAwMA==/MQ==`, {
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Bearer " +
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InNpc3RlbWFjb2JyYW56YSIsIm5iZiI6MTY2Njc5MDQ3NCwiZXhwIjoxNjY2ODc2ODc0LCJpYXQiOjE2NjY3OTA0NzQsImlzcyI6ImFwaS5wYWdvcyIsImF1ZCI6ImFwaS5wYWdvcyJ9.OyzfJ8WWFLt6SVDbmyaNexCIAz8AAX7wUggjQ1VY2Qs",
        },
      })
        .then((response) => response.json())
        .then((json) => {
          console.log("json",json)
        });
    },
  },
});
