import { defineStore } from "pinia";
import axios from "axios";

export const useSorteoStore = defineStore("sorteo", {
    state: () => ({
        dataSorteo: []
    }),
    actions:{
        async resultSorteo() {
            this.dataSorteo = [];

            fetch("https://jsonplaceholder.typicode.com/posts/1")
            .then((response) => response.json())
            .then((json) => {
                console.log("json",json)
            })
              
        }
    }
});