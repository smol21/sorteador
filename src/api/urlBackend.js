export const API_URL_BACKEND = 'http://192.168.200.226:8041/'; //URL TESTING BASE
// export const API_URL_BACKEND = 'http://192.168.200.226:8041/'; //URL QA BASE
// export const API_URL_BACKEND = 'https://www.apismol.com.ve/'; //URL PRODUCCION BASE
export const API_GENERAL = API_URL_BACKEND + 'triplegordoweb.Servicio.general/ServicioTGWebGeneral.svc/ServicioTGWebGeneral/';
export const API_COBRANZA = API_URL_BACKEND + 'triplegordoweb.Servicio.cobranza/ServicioTGWebCobranza.svc/ServicioTGWebCobranza/';
export const API_USUARIO = API_URL_BACKEND + 'triplegordoweb.Servicio.Usuario/ServicioTGWebUsuario.svc/ServicioTGWebUsuario/';