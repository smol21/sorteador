import { createRouter, createWebHashHistory } from "vue-router";
import Home from "@/views/HomeView.vue";
import Login from "@/views/LoginView.vue";
import Horarios from "@/views/HorariosView.vue";

const routes = [
  {
    meta: {
      title: "Login",
    },
    path: "/",
    name: "login",
    component: Login,
  },
  {
    // Document title tag
    // We combine it with defaultDocumentTitle set in `src/main.js` on router.afterEach hook
    meta: {
      title: "Dashboard",
    },
    path: "/dashboard",
    name: "dashboard",
    component: Home,
  },
  {
    meta: {
      title: "Tables",
    },
    path: "/tables",
    name: "tables",
    component: () => import("@/views/TablesView.vue"),
  },
  {
    meta: {
      title: "Forms",
    },
    path: "/forms",
    name: "forms",
    component: () => import("@/views/FormsView.vue"),
  },
  {
    meta:{
      title: "/Horarios",
    },
    path: "/horarios",
    name: "horarios",
    component: Horarios,
  },
  {
    meta:{
      title: "/Sorteo",
    },
    path: "/sorteo",
    name: "sorteo",
    component: () => import("@/views/SorteoView.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return savedPosition || { top: 0 };
  },
});

export default router;
